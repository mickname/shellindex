import argparse
import logging
import sys

import aiohttp_jinja2
import jinja2
from aiohttp import web

from routes import setup_routes


async def init_app(file_root=None):
    app = web.Application()

    app['file_root'] = file_root

    # setup Jinja2 template renderer
    aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader('templates'))

    # setup views and routes
    setup_routes(app)

    return app


def main(argv):
    logging.basicConfig(level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument('--file-root', metavar='ROOT', required=True,
                        help='root directory for served directory indices')
    parser.add_argument('--host', default='localhost', help='interface to bind to')
    parser.add_argument('--port', default=5000, type=int, help='port to bind to')
    args = parser.parse_args()

    app = init_app(args.file_root)

    web.run_app(app, host=args.host, port=args.port)


if __name__ == '__main__':
    main(sys.argv[1:])
