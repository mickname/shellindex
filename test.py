#!venv/bin/python
import asyncio
import contextlib
import logging
import pathlib
import urllib.error
import urllib.request

from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop
from aiohttp import web

import main


@contextlib.contextmanager
def no_log(level=logging.CRITICAL):
    logging.disable(level)
    yield
    logging.disable(logging.NOTSET)


class MyAppTestCase(AioHTTPTestCase):
    traversal_test_paths = ['..', '../', '../..', '../../', '..//']

    async def get_application(self):
        app = await main.init_app()
        # Override the file root for testing to the application directory.
        app['file_root'] = pathlib.Path(__file__).parent.absolute()
        return app

    @unittest_run_loop
    async def test_sanity_check(self):
        '''Test that the index returns with 200 and contains the output for a file listing.
        '''
        resp = await self.client.get("/")
        self.assertEqual(resp.status, 200)
        text = await resp.text()
        # The outputted file listing contain the project files.
        self.assertIn('main.py', text)
        self.assertIn('routes.py', text)
        self.assertIn('views.py', text)

    @unittest_run_loop
    async def test_traversal_vulns(self):
        '''aiohttp client automatically resolves ".." in url in the client side.
            Use another client for testing for traversal.
        '''
        async def test_path(path):
            url = f"http://{self.client.host}:{self.client.port}/{path}"

            loop = asyncio.get_running_loop()

            with no_log(logging.WARNING):
                with self.assertRaises(urllib.error.HTTPError) as context:
                    _ = await loop.run_in_executor(None, lambda: urllib.request.urlopen(url))

            exception = context.exception
            self.assertEqual(exception.code, web.HTTPNotFound.status_code)
            self.assertNotIn('modified', exception.fp.read().decode('utf-8'))

        await asyncio.gather(*[
            test_path(path) for path in self.traversal_test_paths
        ])

    @unittest_run_loop
    async def test_api_invalid_json(self):
        resp = await self.client.post("/_api/shell", data='hahaa')
        self.assertEqual(resp.status, web.HTTPBadRequest.status_code)

    @unittest_run_loop
    async def test_ls_command_traversal_vulns(self):
        async def test_path(path):
            resp = await self.client.post("/_api/shell", json={
                'commandLine': f'ls {path}',
                'PWD': '/',
            })
            json_data = await resp.json()
            self.assertNotIn('html', json_data)

        with no_log(logging.WARNING):
            await asyncio.gather(*[
                test_path(path) for path in self.traversal_test_paths
            ])
