let PWD = null;
window.addEventListener('DOMContentLoaded', function() {
    const prompt_template = document.getElementById('prompt-template');
    const active_prompt = document.getElementById('active-prompt');
    const hidden_input = document.querySelector('#active-prompt input');
    const cursor = document.getElementById('cursor');
    const cursor_character = cursor.querySelector('span');
    const pre_cursor = document.getElementById('pre');
    const post_cursor = document.getElementById('post');
    let command_in_flight = false;
    let cursor_blink_timer = null;

    function submit_line(line) {
        command_in_flight = true;
        new_entry = prompt_template.content.cloneNode(true);
        new_entry.querySelector('.prompt-command').textContent = line;
        active_prompt.parentNode.insertBefore(new_entry, active_prompt);

        // Hide prompt until submitted command has finished.
        active_prompt.style.display = 'none';

        fetch('/_api/shell', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({'commandLine': line, 'PWD': PWD})
        }).then(async (response) => {
            const data = await response.json();
            let output_container = document.createElement('div');
            output_container.classList.add('command-output');

            if ('redirect' in data) {
                window.location.assign(data.redirect);
                return;
            } else if ('text' in data) {
                output_container.textContent = data.text;
            } else if ('html' in data) {
                output_container.innerHTML = data.html;
            } else {
                console.log("You broke it!!");
            }
            active_prompt.parentNode.insertBefore(output_container, active_prompt);
            command_finished();
        });
    }

    function command_finished() {
        command_in_flight = false;
        active_prompt.style.display = 'inline';
        cursor.scrollIntoView();
    }

    function get_initial_pwd() {
        /* hax :-) */
        const prompt = document.querySelector('.prompt-path');
        return prompt.textContent.replace(/^~\/web_root/, '');
    }

    PWD = get_initial_pwd();

    hidden_input.value = '';

    function adjust_prompt() {
        const val = hidden_input.value;
        const cursor_pos = hidden_input.selectionStart;
        pre_cursor.textContent = val.substring(0, cursor_pos);
        if (hidden_input.selectionStart == val.length) { /* "cursor at end" */
            cursor_character.textContent = ' ';
            post_cursor.style.display = 'none';
        } else {
            cursor_character.textContent = val[cursor_pos];
            post_cursor.style.display = 'inline';
            post_cursor.textContent = val.substring(cursor_pos);
        }
    }

    function disable_blink() {
        if (cursor_blink_timer !== null) {
            window.clearTimeout(cursor_blink_timer);
        } else {
            cursor.classList.remove('blink');
        }
        cursor_blink_timer = window.setTimeout(function() {
            cursor.classList.add('blink');
            cursor_blink_timer = null;
        }, 200);
    }

    document.addEventListener('keydown', function(e) {
        const k = e.key;
        if (k == 'Enter') {
            if (!command_in_flight) {
                submit_line(hidden_input.value);
                hidden_input.value = '';
            }
        } else if (k == 'Tab') {
            // TODO: tab complete
            e.preventDefault();
        }

        hidden_input.focus();
        window.setTimeout(adjust_prompt, 1);
        disable_blink();
    });
});
