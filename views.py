import json
import logging
import pathlib
import shlex

from markupsafe import Markup
import aiohttp_jinja2
from aiohttp import web
from yarl import URL

import commands

from utils import (
    path_inside_root,
)


async def serve_file(request, file_path):
    # TODO: encoding for txt, sendfile
    return web.FileResponse(file_path)


async def serve_index(request, relative_path):
    sort_args = []
    try:
        ordering = request.rel_url.query['o']
        sort_args = [{
            'sa': '-Sr',
            'sd': '-S',
            'ma': '-tr',
            'md': '-t',
            # 'na': ''
            'nd': '-r',
        }[ordering]]
    except KeyError:
        pass

    full_args = ['-lha'] + sort_args

    pwd = pathlib.PurePosixPath(relative_path)

    result = await commands.command['ls'](request, pwd, full_args)

    try:
        output = Markup(result['html'])
        pwd_fake = str(pathlib.PurePosixPath('~/web_root') / pwd)
    except KeyError:
        # Aka: command returned textual error not found
        output = result['text']
        pwd_fake = str('~/web_root')

    return aiohttp_jinja2.render_template(
        'main.html',
        request,
        {
            'command_output': output,
            'command': ' '.join(['ls'] + full_args),
            'pwd': pwd_fake,
            'title': f'Index of /{relative_path.rstrip("/")}',
        },
        status=result.get('status_code', 200)
    )


async def path_handler(request):
    relative_url_path = request.match_info['relative_url_path']
    abs_file_path = (pathlib.Path(request.app['file_root']) / relative_url_path).resolve()

    # print("file path:", abs_file_path)

    if not path_inside_root(root=request.app['file_root'], path=abs_file_path):
        logging.warning("Requested web path '{}' not inside file root!".format(relative_url_path))
        raise web.HTTPNotFound()

    if abs_file_path.is_file():
        return await serve_file(request, abs_file_path)
    elif abs_file_path.is_dir():
        # If the url is for a directory, redirect to canonical version with ending slash.
        if relative_url_path and not relative_url_path.endswith('/'):
            raise web.HTTPFound(URL.build(
                path=f'/{relative_url_path}/',
                query_string=request.rel_url.query_string
            ))
    return await serve_index(request, relative_url_path)


async def shell_command(request):
    try:
        parsed_json = await request.json()
        command_line = parsed_json['commandLine']
        pwd = parsed_json['PWD']
        pwd = pwd.lstrip('/')
    except (json.decoder.JSONDecodeError, KeyError):
        raise web.HTTPBadRequest(text='Invalid json.')

    try:
        parsed = list(shlex.shlex(command_line, posix=True, punctuation_chars=True))
    except ValueError as e:
        return web.json_response({'text': f'splash: {str(e)}'})

    logging.info("Parsed command: %s, PWD: %s", parsed, pwd)

    tokens_set = set(parsed)
    unsupported_tokens = ['|', '*', '<', '>', ';', '(', ')', '&']
    for t in unsupported_tokens:
        if t in tokens_set:
            return web.json_response({'text': "splash: This isn't a real shell. Dumbass."})
    try:
        command = parsed[0]
    except IndexError:
        return web.json_response({'text': ''})
    args = parsed[1:]

    try:
        resp = await commands.command[command](request, pwd, args)
        return web.json_response(resp)
    except KeyError:
        return web.json_response({'text': f'splash: {command}: command not found'})
