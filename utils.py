import datetime
import os.path
import pathlib


def path_inside_root(root, path):
    path = pathlib.PurePath(path)
    return pathlib.PurePath(root) in [path] + list(path.parents)


def size_to_human_readable(size):
    '''Converts number of bytes to a tuple of (number, unit) where unit is
    kibi/mebi/... etc bytes.
    '''
    for suffix in [None, 'K', 'M', 'G', 'T']:
        if size < 1024:
            break
        size /= 1024
    return (size, suffix)


def size_to_str(size):
    size, suffix = size_to_human_readable(size)
    if suffix is None:
        return "{:d}".format(size)
    else:
        return "{:.1f}{}".format(size, suffix)


def format_time_str(time):
    '''Formats a datetime object as in /ls/ output, for entries in this year show month+time, else
    show month + year. Output should be constant length of 12 characters.
    '''
    this_year = datetime.datetime.now().year
    if time.year == this_year:
        # %b = Month as locale’s abbreviated name.
        return time.strftime('%b %d %H:%M')
    else:
        return time.strftime('%b %d  %Y')


def guess_file_type(name):
    _, ext = os.path.splitext(name)
    if ext in {'.png', '.jpg', '.jpeg', '.bmp', '.svg', '.gif'}:
        return 'image'
    elif ext in {'.webm', '.mp4', '.avi'}:
        return 'video'
    elif ext in {'.flac', '.opus', '.ogg', '.mp3'}:
        return 'audio'
    elif ext in {'.tar', '.tar.gz', '.tar.xz', '.tar.bzip2', '.gz', '.xz', '.bzip2', '.zip', '.rar'}:
        return 'archive'
    else:
        return None
