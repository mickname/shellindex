from datetime import datetime
import asyncio
import getopt
import logging
import os
import os.path
import pathlib
import urllib.parse

import aiohttp_jinja2

from utils import (
    format_time_str,
    guess_file_type,
    path_inside_root,
    size_to_str,
)


async def cowsay_imp(request, pwd, arguments):

    saying = ' '.join(arguments)
    fl = len(saying)
    prefix = min(fl // 2 + 3, 10) * " "
    lines = [
        "  " + fl * "_",
        "< " + saying + " >",
        "  " + fl * "-",
        prefix + "\\   ^__^",
        prefix + " \\  (oo)\\_______",
        prefix + "    (__)\\       )\\/\\",
        prefix + "        ||----w |",
        prefix + "        ||     ||",
    ]
    return {'text': '\n'.join(lines)}


async def help_impl(request, pwd, arguments):
    known_commands = ' '.join(sorted(command.keys()))
    return {
        'text': (
            "mikkis.net splash, version 1.0\n"
            f"Built-ins: {known_commands}\n"
        )
    }


async def echo_impl(request, pwd, arguments):
    return {'text': ' '.join(arguments)}


async def uptime_impl(request, pwd, arguments):
    proc = await asyncio.create_subprocess_exec('uptime', stdout=asyncio.subprocess.PIPE)
    uptime_text = (await proc.stdout.readline()).decode('utf-8')
    await proc.wait()
    return {'text': uptime_text}


async def sleep_impl(request, pwd, arguments):
    USAGE = (
        "Usage: sleep NUMBER\n"
        "   or: sleep --help"
    )
    try:
        opts, positional = getopt.getopt(arguments, '', ['help'])
        await asyncio.sleep(float(arguments[0]))
    except (getopt.GetoptError, KeyError, IndexError, ValueError):
        return {'text': USAGE}

    return {'text': ''}


async def cd_impl(request, pwd, arguments):
    USAGE = (
        "Usage: cd [dir]\n"
        "   or: cd --help"
    )
    try:
        opts, positional = getopt.getopt(arguments, '', ['help'])
    except (getopt.GetoptError, KeyError, IndexError, ValueError):
        return {'text': USAGE}

    for opt, _ in opts:
        if opt == '--help':
            return {'text': USAGE}

    if len(arguments) > 2:
        return {'text': 'splash: cd: too many arguments'}

    try:
        destination = urllib.parse.quote(arguments[0])
        if destination[-1] != '/':
            destination += '/'
    except IndexError:
        destination = '/'

    return {'redirect': destination}


async def ls_impl(request, pwd, arguments):
    ''' opts:
        -l: list
        -h: human readable size
        -a: "all"
        -t: sort by mod time, newest first
        -S: sort by size, largest first
        -r: reverse the sorting order
        --help

        and the relative path
    '''

    opts, positional = getopt.getopt(arguments, 'ahlrSt', ['help'])

    if len(positional) > 1:
        return {'text': "splash: ls: too many arguments. This isn't a real shell, you know?"}

    list_format = False
    dot_files = False

    reverse_sort = False
    size_fmt = str
    sort_fn = (lambda entry: entry['name'])
    sort_col = 'name'
    sort_dir = 'asc'
    for opt, value in opts:
        if opt == '-l':
            list_format = True
        elif opt == '-h':
            size_fmt = size_to_str
        elif opt == '-a':
            dot_files = True
        elif opt == '-t':
            sort_fn = (lambda entry: -entry['modified'].timestamp())
            sort_col = 'time'
            sort_dir = 'desc'
        elif opt == '-S':
            sort_fn = (lambda entry: -entry['size_bytes'])
            sort_col = 'size'
            sort_dir = 'desc'
        elif opt == '-r':
            reverse_sort = True

    if reverse_sort:
        sort_dir = 'desc' if sort_dir == 'asc' else 'asc'

    # only one path supported
    try:
        relative_path = pwd / pathlib.PurePath(positional[0])
    except IndexError:
        relative_path = pwd / pathlib.PurePath('.')
    absolute_path = (pathlib.Path(request.app['file_root']) / relative_path).resolve()

    if not path_inside_root(root=request.app['file_root'], path=absolute_path):
        logging.warning(f"ls command path: {relative_path}")
        return {
            'text': f"ls: cannot open directory '{relative_path}': Permission denied",
            'status_code': 403,
        }

    file_list = []

    def parse_entry(entry):
        stat = entry.stat()
        entry_type = ''
        if entry.is_dir():
            entry_type = 'dir'
        else:
            entry_type = guess_file_type(entry.name) or ''

        if entry.name == 'no-index':
            raise PermissionError()

        file_mod = datetime.fromtimestamp(stat.st_mtime)
        url = str(entry.name)
        if entry.is_dir() and not url.endswith('/'):
            url += '/'
        return {
            'size': size_fmt(stat.st_size),
            'size_bytes': stat.st_size,
            'modified': file_mod,
            'modified_str': format_time_str(file_mod),
            'name': entry.name,
            'quote_char': "'" if ' ' in entry.name else " ",
            'type': entry_type,
            'url': url,
            'is_dir': entry.is_dir(),  # (for sort)
        }
    try:
        with os.scandir(absolute_path) as entries:
            for entry in entries:
                file_list.append(parse_entry(entry))
    except PermissionError:
        return {
            'text': f"ls: cannot open directory '{relative_path}': Permission denied",
            'status_code': 403,
        }
    except FileNotFoundError:
        return {
            'text': f"ls: cannot access '{relative_path}': No such file or directory",
            'status_code': 404,
        }
    except NotADirectoryError:
        dot_files = False
        file_list = [parse_entry(absolute_path)]

    # First sort by the desired column
    file_list.sort(key=sort_fn, reverse=reverse_sort)
    # Secondly, sort by is_dir to always keep dirs at the front.
    file_list.sort(key=lambda entry: not entry['is_dir'])

    if dot_files:
        # Add the directories . and .. as special cases. os.scandir does not include them.
        dots = [{
            'size': size_fmt(stat.st_size),
            'size_bytes': stat.st_size,
            'modified_str': format_time_str(datetime.fromtimestamp(stat.st_mtime)),
            'name': name,
            'quote_char': ' ',
            'type': 'dir',
            'url': name,
        } for stat, name in [(absolute_path.stat(), '.'), (absolute_path.parents[0].stat(), '..')]]
        file_list = dots + file_list

    if list_format:
        longest_by_size = max(file_list, key=lambda item: len(str(item['size'])))
        try:
            relative_link = positional[0] + ('/' if positional[0][-1] != '/' else '')
        except IndexError:
            relative_link = ''
        return {'html': aiohttp_jinja2.render_string('ls_list.html', request, {
            'file_list': file_list,
            'size_col_len': len(str(longest_by_size['size'])),
            'sorting': {
                'col': sort_col,
                'dir': sort_dir,
            },
            'relative_link': relative_link,
        })}
    else:
        default_term_width = 110
        longest_by_name = max(file_list, key=lambda item: len(item['name']))
        longest_name = len(longest_by_name['name']) + 2
        cols = max(1, default_term_width // longest_name)
        return {'html': aiohttp_jinja2.render_string(
            'ls_default.html', request, {
                'file_list': file_list,
                'num_columns': cols,
            })}


async def cat_impl(request, pwd, arguments):
    USAGE = (
        "Usage: cat FILE\n"
        "   or: cat --help\n"
        "Truncate a file to output. No concatenating."
    )

    try:
        opts, positional = getopt.getopt(arguments, '', ['help'])
    except (getopt.GetoptError, KeyError, IndexError, ValueError):
        return {'text': USAGE}

    for opt, _ in opts:
        if opt == '--help':
            return {'text': USAGE}

    if len(arguments) > 1:
        return {'text': "cat: too many arguments. This isn't a real shell, you know?"}

    # only one path supported
    try:
        relative_path = pwd / pathlib.PurePath(positional[0])
    except IndexError:
        return {'text': USAGE}
    absolute_path = (pathlib.Path(request.app['file_root']) / relative_path).resolve()

    if not path_inside_root(root=request.app['file_root'], path=absolute_path):
        logging.warning(f"cat command path: {relative_path}")
        return {'text': f"cat: '{relative_path}': Permission denied"}

    try:
        with open(absolute_path, 'rb') as f:
            txt = f.read(1000).decode('utf-8', errors='backslashreplace')
            truncated = len(f.read(1)) == 1
            return {'html': aiohttp_jinja2.render_string(
                'cat.html', request, {'txt': txt, 'truncated': truncated}
            )}
    except IsADirectoryError:
        return {'text': f"cat: '{relative_path}': Is a directory"}
    except FileNotFoundError:
        return {'text': f"cat: '{relative_path}': No such file or directory"}

command = {
    'cat': cat_impl,
    'cd': cd_impl,
    'cowsay': cowsay_imp,
    'echo': echo_impl,
    'help': help_impl,
    'ls': ls_impl,
    'sleep': sleep_impl,
    'uptime': uptime_impl,
}
