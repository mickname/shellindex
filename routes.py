import pathlib

from views import path_handler, shell_command


PROJECT_ROOT = pathlib.Path(__file__).parent


def setup_routes(app):
    app.router.add_static('/_static/', path=(PROJECT_ROOT / 'static'), name='static')
    app.router.add_post('/_api/shell', shell_command)
    app.router.add_get('/{relative_url_path:.*}', path_handler)
